# Autofocus Test Software

Aplicação que virtualiza o processo de autofoco do maia-firmware.

### Usage

Instalar as bibliotecas necessárias com pip.
> pip install requirements.txt

Para rodar o script, providencie o caminho para um diretório contendo uma stack focal e o método de avaliação da qualidade da imagem.

> python autofocus.py --input path_to_focal_stack --method iqa_method

Métodos atualmente implementados:
    'JpegSize',
    'Laplacian', 
    'CMSL',
    'SML',
    'GLV',
    'Tenengrad',
    'Jaehne',
    'DSharpness',
    'CalContrast',
    'FastFourier',
    'RingDifference',
    'DetectLines',
    'DetectCircles',
    'LaplacianStd'

![picture](mock_autofocus.png)

### Nota para desenvolvedores
O script se baseia na classe ImageQualityAssessment, no diretório iqa_methods. Todos os métodos de avaliação de qualidade de imagem devem ser adicionados e carregados na definição da classe. <br>
O otimizador atual é baseado no Fractioner que se encontra em produção do MAIA-05. Para alterar ou adicionar novos otimizadores, deve ser alterado o arquivo optimization/optimizer.py, adicionando uma nova classe ou alterando a existente.