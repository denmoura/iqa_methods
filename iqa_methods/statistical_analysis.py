import os
import pandas as pd
import matplotlib.pyplot as plt
from iqa_methods import ImageQualityAssessment

Analyzer = ImageQualityAssessment()
Methods = Analyzer.methods

class Compare_Stacks():
    def __init__(self, dataframe):
        self.results = dataframe
        self.methods = Methods

    def show_boxplot(self, df):
        ax = df.plot.box()
        plt.xticks(rotation=40)
        plt.show()
    
    def save_boxplot(self, df, method):
        ax = df.plot.box()
        plt.xticks(rotation=40)
        plt.savefig(os.path.join(output_dir, "{}_boxplot.png".format(method)))

    def plot_boxplots(self, df):
        for method in self.results.keys():
            print('Showing results for {}'.format(method))
            self.show_boxplot(self.results[method])
            print('lower 5% values for {} at in-sample focus at 10X: {}.'.format(method, self.results[method]['Focadas10X'].quantile(q=0.05)))
            print('higher 95% values for {} at coverslip focus at 10X: {}.'.format(method, self.results[method]['laminula10X'].quantile(q=0.95)))

            print('lower 5% values for {} at in-sample focus at 40X: {}.'.format(method, self.results[method]['Focadas40X'].quantile(q=0.05)))
            print('higher 95% values for {} at coverslip focus at 40X: {}.'.format(method, self.results[method]['laminula40X'].quantile(q=0.05)))
            print('\n')
    
    def show_statistics(self):
        for method in self.results.keys():
            print('Showing results for {}'.format(method))
            print('lower 5% values for {} at in-sample focus at 10X: {}.'.format(method, self.results[method]['Focadas10X'].quantile(q=0.05)))
            print('higher 95% values for {} at coverslip focus at 10X: {}.'.format(method, self.results[method]['laminula10X'].quantile(q=0.95)))

            print('lower 5% values for {} at in-sample focus at 40X: {}.'.format(method, self.results[method]['Focadas40X'].quantile(q=0.05)))
            print('higher 95% values for {} at coverslip focus at 40X: {}.'.format(method, self.results[method]['laminula40X'].quantile(q=0.05)))
            print('\n')

    def save_boxplots(self, df):
        for method in self.results.keys():
            self.save_boxplot(self.results[method], method)