# from iqa_methods.skimage_edge import prewitt
import cv2
import numpy as np
from iqa_methods.preprocess import image_preprocess
from iqa_methods.sampling_methods import plot_focal_squares
from iqa_methods import original_methods, laplacian, student, shape_detection

class ImageQualityAssessment():
    def __init__(self):
        self.methods = {
            'JpegSize': original_methods.jpegsize,
            'Laplacian': laplacian.variance_of_laplacian,
            'CMSL': student.CMSL,
            'SML': student.SML,
            'GLV': student.GLV,
            'Tenengrad': student.tenengrad1,
            'Jaehne': student.jaehne,
            'DSharpness': student.derivative_sharpness,
            'CalContrast': student.cal_contrast,
            'FastFourier': original_methods.FastFourierTransformMeasure,
            'RingDifference': original_methods.RingDifferenceFilterVarianceMeasure,
            'DetectLines': shape_detection.detect_lines,
            'DetectCircles': shape_detection.detect_circles,
            'LaplacianStd': laplacian.LaplacianStd,
            'Canny': original_methods.Canny
        }

    def _preprocess(self, image, sampling_method):
        return image_preprocess(image, sampling_method)
    #     # self.img = cv2.imread(image, 1)
    #     gray = cv2.cvtColor(np.uint8(self.img), cv2.COLOR_BGR2GRAY)

    #     # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    #     # gray = clahe.apply(gray)

    #     # gray = cv2.equalizeHist(gray)
    #     h, w, _ = self.img.shape
        

    #     if sampling_method == 'focal_squares':    
    #         focal_squares = [
    #             gray[round(h*0.1):round(h * 0.15), round(w*0.1):round(w * 0.15)],
    #             gray[round(h*0.1):round(h * 0.15), round(w*0.475):round(w * 0.525)],
    #             gray[round(h*0.1):round(h * 0.15), round(w*0.85):round(w * 0.9)],
                
    #             gray[round(h*0.475):round(h * 0.525), round(w*0.1):round(w * 0.15)],
    #             gray[round(h*0.475):round(h * 0.525), round(w*0.475):round(w * 0.525)],
    #             gray[round(h*0.475):round(h * 0.525), round(w*0.85):round(w * 0.9)],

    #             gray[round(h*0.85):round(h * 0.90), round(w*0.1):round(w * 0.15)],
    #             gray[round(h*0.85):round(h * 0.90), round(w*0.475):round(w * 0.525)],
    #             gray[round(h*0.85):round(h * 0.90), round(w*0.85):round(w * 0.9)]
    #         ]

    #     elif sampling_method == 'large_centroid':
    #         focal_squares  = [
    #             gray[round(h*0.25):round(h*0.75), round(w*0.25):round(w*0.75)]
    #         ]


    #     elif sampling_method == 'central_point':
    #         focal_squares = [
    #             gray[round(h*0.45):round(h*0.55), round(w*0.45):round(w*0.55)]
    #         ]

    #     else:
    #         focal_squares = [gray]

    #     return focal_squares        

    # def plot_focal_squares(self, image):
    #     self.img = cv2.imread(image, 1)     
    #     # gray = cv2.cvtColor(np.array(self.img), cv2.COLOR_BGR2GRAY)        
    #     # # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    #     # gray = clahe.apply(gray)

    #     h, w, _ = self.img.shape
    #     # squares = [
    #     #     [(round(w*0.1), round(h*0.1)), (round(w * 0.15), round(h * 0.2))],
    #     #     [(round(w*0.1), round(h*0.450)), (round(w * 0.15), round(h * 0.550))],
    #     #     [(round(w*0.1), round(h*0.80)), (round(w * 0.15), round(h * 0.90))],

    #     #     [(round(w*0.475), round(h*0.1)), (round(w * 0.525), round(h * 0.20))],
    #     #     [(round(w*0.475), round(h*0.450)), (round(w * 0.525), round(h * 0.550))],
    #     #     [(round(w*0.475), round(h*0.8)), (round(w * 0.525), round(h * 0.90))],

    #     #     [(round(w*0.85), round(h*0.1)), (round(w * 0.90), round(h * 0.20))],
    #     #     [(round(w*0.85), round(h*0.450)), (round(w * 0.90), round(h * 0.550))],
    #     #     [(round(w*0.85), round(h*0.8)), (round(w * 0.90), round(h * 0.90))],
    #     # ]

    #     # squares = [
    #     #         [(round(w*0.45), round(h*0.45)), (round(w*0.55), round(h*0.55))]
    #     #     ]

    #     squares  = [
    #             [(round(w*0.25), round(h*0.25)), (round(w*0.75), round(h*0.75))]
    #         ]

    #     for square in squares:
    #         t1, t2 = square
    #         cv2.rectangle(self.img, t1, t2, (0, 0, 255), 2)

    #     cv2.imwrite('large_centroid.jpg', self.img)
    #     # cv2.waitKey(0)
    #     # cv2.destroyAllWindows()

    def method_call(self, method, square):

        return self.methods[method](square)

    def analyze(self, image, method, sampling_method):
        self.img = cv2.imread(image, 1)

        focal_squares = self._preprocess(self.img, sampling_method)

        res = []
        for square in focal_squares:
            res.append(self.method_call(method, square))

        return np.median(res)