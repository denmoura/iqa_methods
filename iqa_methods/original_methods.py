import cv2 as cv
import numpy as np

def FastFourierTransformMeasure(img):
    h, w = img.shape
    y = np.fft.fft2(img)
    
    output = np.abs(np.absolute(y) * np.angle(y))
    return output.sum() / (h * w)

def jpegsize(img):
    _, byte_array = cv.imencode('.jpg', img)
    return byte_array.shape[0]


def RingDifferenceFilterVarianceMeasure(img):
    kernel = np.array([[0, 1, 1, 1, 0],
                        [1, 0, 0, 0, 1],
                        [1, 0, -12, 0, 1],
                        [1, 0, 0, 0, 1],
                        [0, 1, 1, 1, 0]])
    
    return cv.filter2D(img, -1, kernel).var()

def Canny(img):
    canny_img = cv.Canny(img, 0, 100)
    return float(np.mean(canny_img))