import cv2
import numpy as np

def image_preprocess(img, sampling_method):
        # self.img = cv2.imread(image, 1)
        gray = cv2.cvtColor(np.uint8(img), cv2.COLOR_BGR2GRAY)

        # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        # gray = clahe.apply(gray)

        # gray = cv2.equalizeHist(gray)
        h, w, _ = img.shape
        

        if sampling_method == 'focal_squares':    
            focal_squares = [
                gray[round(h*0.1):round(h * 0.15), round(w*0.1):round(w * 0.15)],
                gray[round(h*0.1):round(h * 0.15), round(w*0.475):round(w * 0.525)],
                gray[round(h*0.1):round(h * 0.15), round(w*0.85):round(w * 0.9)],
                
                gray[round(h*0.475):round(h * 0.525), round(w*0.1):round(w * 0.15)],
                gray[round(h*0.475):round(h * 0.525), round(w*0.475):round(w * 0.525)],
                gray[round(h*0.475):round(h * 0.525), round(w*0.85):round(w * 0.9)],

                gray[round(h*0.85):round(h * 0.90), round(w*0.1):round(w * 0.15)],
                gray[round(h*0.85):round(h * 0.90), round(w*0.475):round(w * 0.525)],
                gray[round(h*0.85):round(h * 0.90), round(w*0.85):round(w * 0.9)]
            ]

        elif sampling_method == 'large_centroid':
            focal_squares  = [
                gray[round(h*0.25):round(h*0.75), round(w*0.25):round(w*0.75)]
            ]


        elif sampling_method == 'central_point':
            focal_squares = [
                gray[round(h*0.45):round(h*0.55), round(w*0.45):round(w*0.55)]
            ]

        else:
            focal_squares = [gray]

        return focal_squares     