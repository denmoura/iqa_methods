import cv2
import numpy as np
import sys
import os

def detect_lines(img):
    # img = cv2.imread(image)
    # img = img[100:540, 55:650]
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(img, 100, 200, apertureSize=3)
    kernel = np.ones((5,5),np.uint8)

    edges = cv2.dilate(
      edges,
      kernel,
      iterations=1
  )
    edges = cv2.erode(
      edges,
      kernel,
      iterations=1
  )


    # cv2.imwrite('edges_{}'.format(image), edges)
    minLineLength = 100
    maxLineGap = 10
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength, maxLineGap)
    # for i in range(len(lines)):
    #     for x1, y1, x2, y2 in lines[i]:
    #         cv2.line(img,(x1, y1), (x2, y2), (0, 255, 0), 2)

    # return img
    if lines is not None:
        return len(lines)

    else:
        return 0

def detect_circles(img):
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1.2, 50)

    if circles is not None:
        return len(circles)

    else:
        return 0
