import cv2
import sys
# from PIL import Image
import time
import io
import numpy as np

def variance_of_laplacian(img):
    return cv2.Laplacian(np.array(img), cv2.CV_64F).var()

def gray_laplacian(img):
    gray = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
    laplacian = cv2.Laplacian(gray, cv2.CV_64F)
    return laplacian.var()

def reduce_laplacian(img):
    w, h = img.size
    reduced_img = img.crop((w/4, h/4, w - (w/4), h - (h/4)))

    # gray = cv2.cvtColor(np.array(reduced_img), cv2.COLOR_BGR2GRAY)
    laplacian = cv2.Laplacian(np.array(reduced_img), cv2.CV_64F)
    return laplacian.var()

def reduce_gray_laplacian(img):
    w, h = img.size
    reduced_img = img.crop((w/4, h/4, w - (w/4), h - (h/4)))

    gray = cv2.cvtColor(np.array(reduced_img), cv2.COLOR_BGR2GRAY)
    laplacian = cv2.Laplacian(gray, cv2.CV_64F)
    return laplacian.var()

def LaplacianStd(srcImage):
    # img = cv2.imread(srcImage)
    # gray = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
    """ Compute sharpness score for automatic focus """
    # adjustedImage = AdjustContrast(gray)
    filteredImage = cv2.GaussianBlur(srcImage, (3, 3), 0, 0)

    kernel_size = 11
    scale = 1
    delta = 0
    #lplImage = cv2.Laplacian(filteredImage, cv2.CV_64F, ksize=kernel_size, scale=scale, delta=delta)
    lplImage = cv2.Laplacian(filteredImage, cv2.CV_16S, ksize=kernel_size, scale=scale, delta=delta)

    # converting back to CV_8U generate the standard deviation
    #absLplImage = cv2.convertScaleAbs(lplImage)

    # get the standard deviation of the absolute image as input for the sharpness score
    # (mean, std) = cv2.meanStdDev(absLplImage)
    (mean, std) = cv2.meanStdDev(lplImage)

    return std[0][0]**2


if __name__ == '__main__':
    img_path = sys.argv[1]
    img = Image.open(img_path)

    start_time_1 = time.time()
    print(variance_of_laplacian(img))
    end_time_1 = time.time()

    start_time_2 = time.time()
    print(gray_laplacian(img))
    end_time_2 = time.time()

    start_time_3 = time.time()
    print(reduce_laplacian(img))
    end_time_3 = time.time()

    start_time_4 = time.time()
    print(reduce_gray_laplacian(img))
    end_time_4 = time.time()


    print("First algorithm finished in {} seconds.".format(end_time_1 - start_time_1))
    print("Second algorithm finished in {} seconds.".format(end_time_2 - start_time_2))
    print("Third algorithm finished in {} seconds.".format(end_time_3 - start_time_3))
    print("Fourth algorithm finished in {} seconds.".format(end_time_4 - start_time_4))


