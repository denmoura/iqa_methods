import cv2
import numpy as np

def plot_focal_squares(image, output_name):
    img = cv2.imread(image, 1)     
    # gray = cv2.cvtColor(np.array(self.img), cv2.COLOR_BGR2GRAY)        
    # # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    # gray = clahe.apply(gray)

    h, w, _ = img.shape
    # squares = [
    #     [(round(w*0.1), round(h*0.1)), (round(w * 0.15), round(h * 0.2))],
    #     [(round(w*0.1), round(h*0.450)), (round(w * 0.15), round(h * 0.550))],
    #     [(round(w*0.1), round(h*0.80)), (round(w * 0.15), round(h * 0.90))],

    #     [(round(w*0.475), round(h*0.1)), (round(w * 0.525), round(h * 0.20))],
    #     [(round(w*0.475), round(h*0.450)), (round(w * 0.525), round(h * 0.550))],
    #     [(round(w*0.475), round(h*0.8)), (round(w * 0.525), round(h * 0.90))],

    #     [(round(w*0.85), round(h*0.1)), (round(w * 0.90), round(h * 0.20))],
    #     [(round(w*0.85), round(h*0.450)), (round(w * 0.90), round(h * 0.550))],
    #     [(round(w*0.85), round(h*0.8)), (round(w * 0.90), round(h * 0.90))],
    # ]

    # squares = [
    #         [(round(w*0.45), round(h*0.45)), (round(w*0.55), round(h*0.55))]
    #     ]

    squares  = [
            [(round(w*0.25), round(h*0.25)), (round(w*0.75), round(h*0.75))]
        ]

    for square in squares:
        t1, t2 = square
        cv2.rectangle(img, t1, t2, (0, 0, 255), 2)

    cv2.imwrite('{}.jpg'.format(output_name), img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()