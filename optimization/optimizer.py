import cv2
import numpy as np

class Fractioner():
    def __init__(self):
        pass

    def load_img(self, image, sharpen=False):
        img = cv2.imread(image)
        gray = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
        if sharpen:
            kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
            gray = cv2.filter2D(gray, -1, kernel)

        return gray

    def find_focus(self, fn, stack):
        fractions = {}
        z_scores = {}
        long_step = 3
        short_step = 1
        for z in np.arange(0, len(stack), long_step):
            z_score = fn(self.load_img(stack[z], sharpen=True))
            fractions[z] = z_score

        chosen_fraction = max(fractions, key=fractions.get)
        
        lower_boundary = chosen_fraction - (short_step * 5)
        if lower_boundary < 0:
            lower_boundary = 0
        
        higher_boundary = chosen_fraction + (short_step * 5)

        if higher_boundary > len(stack) + 1:
            higher_boundary = len(stack)

        new_boundaries = (lower_boundary, higher_boundary)

        print("Chosen fraction: {}".format(chosen_fraction))
        print("Focus seems to be within: {} and {}".format(new_boundaries[0], new_boundaries[1]))
        
        for z in range(new_boundaries[0], new_boundaries[1], short_step):
            z_score = fn(self.load_img(stack[z], sharpen=True))
            z_scores[z] = z_score

        chosen_focus = max(z_scores, key=z_scores.get)
        score = z_scores[chosen_focus]

        return chosen_focus, score, z_scores