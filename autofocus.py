import os
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from optimization.optimizer import Fractioner
from iqa_methods import ImageQualityAssessment

Analyzer = ImageQualityAssessment()
Optimizer = Fractioner()

parser = ArgumentParser()
parser.add_argument('-i', '--input', required=True,
    help="Provide an input path for the stack directory.")
parser.add_argument('-m', '--method', required = True,
    help="Choose a valid method for the autofocus. Current valid methods: {}".format(Analyzer.methods))

args = vars(parser.parse_args())

# def find_focus(fn, stack):
#     fractions = {}
#     z_scores = {}
#     long_step = 3
#     short_step = 1
#     for z in np.arange(0, len(stack), long_step):
#         z_score = fn(load_img(stack[z]))
#         fractions[z] = z_score

#     chosen_fraction = max(fractions, key=fractions.get)
    
#     lower_boundary = chosen_fraction - (short_step * 5)
#     if lower_boundary < 0:
#         lower_boundary = 0
    
#     higher_boundary = chosen_fraction + (short_step * 5)

#     if higher_boundary > len(stack) + 1:
#         higher_boundary = len(stack)

#     new_boundaries = (lower_boundary, higher_boundary)

#     print("Chosen fraction: {}".format(chosen_fraction))
#     print("Focus seems to be within: {} and {}".format(new_boundaries[0], new_boundaries[1]))
    
#     for z in range(new_boundaries[0], new_boundaries[1], short_step):
#         z_score = fn(load_img(stack[z]))
#         z_scores[z] = z_score

#     chosen_focus = max(z_scores, key=z_scores.get)
#     score = z_scores[chosen_focus]

#     return chosen_focus, score, z_scores


stack_path = args['input']
method = Analyzer.methods[args['method']]
stack = [os.path.join(stack_path, stack) for stack in os.listdir(stack_path)]
chosen_img, score, z_scores = Optimizer.find_focus(method, stack)

print("Chosen image: {}, {}".format(chosen_img, stack[chosen_img]))
print("Score: {}".format(score))
print(z_scores)

# if __name__ == '__main__':
#     stack_path = sys.argv[1]
    
#     # methods = {
#     #     "DerivativeSharpness": DerivativeSharpness,
#     #     "LaplacianVariance": LaplacianVariance,
#     #     "LaplacianStd": LaplacianStd,
#     #     "CalculateHistogram": CalculateHistogram
#     # }
    
#     method = Analyzer.methods[sys.argv[2]]

#     stack = [os.path.join(stack_path, stack) for stack in os.listdir(stack_path)]

#     chosen_img, score, z_scores = find_focus(method, stack)

#     print("Chosen image: {}, {}".format(chosen_img, stack[chosen_img]))
#     print("Score: {}".format(score))
#     print(z_scores)