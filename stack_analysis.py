import os
import sys
import numpy as np
import pandas as pd
from argparse import ArgumentParser
from iqa_methods import ImageQualityAssessment
from iqa_methods.statistical_analysis import Compare_Stacks

Analyzer = ImageQualityAssessment()

parser = ArgumentParser()
parser.add_argument("-i", "--input", required=True,
    help="Path to an image folder for a specific image class")
parser.add_argument("-sm", "--sampling_method", required=False,
    help = "Provide a sampling method. Current methods: large_centroid, central_point, and focal_squares.")
parser.add_argument("-o", "--output", required=False,
    help = "Provide an output directory.")
parser.add_argument("-n", "--name", required=False,
    help = "Provida a basename for the output files.")
parser.add_argument("-p", "--plot", required=False, action="store_true",
    help = "Add this argument to plot the boxplots comparing each method.")
parser.add_argument("--stats", required=False, action="store_true",
    help = "Add this argument to print the statistics of the compared methods.")
parser.add_argument("-s", "--save", required=False, action="store_true",
    help = "add this argument to save the output boxplots.")

args = vars(parser.parse_args())

def get_images(img_folder):
    images = []
    for img_file in os.listdir(img_folder):
        img_path = os.path.join(img_folder, img_file)
        images.append(img_path)
    return images

def get_results(images, sampling_method):
    results = {}
    for IQA_Method in [method for method in Analyzer.methods.keys()]:
        method_results = [Analyzer.analyze(img, IQA_Method, sampling_method) for img in images]
        results[IQA_Method] = method_results
        
    return results

def save_results(output_dir, base_name):
    res_df = pd.DataFrame.from_dict(results, orient = 'index').transpose()
    res_df.to_csv(os.path.join(output_dir, '{}_{}_results.csv'.format(base_name, sampling_method)), index= None)

    return res_df

sampling_method = args.get("sampling_method", "")
output_dir = args.get("output", str(os.path.join(os.getcwd(), 'output/')))
if not output_dir:
    output_dir = os.path.join(os.getcwd(), 'output/')
base_name = args.get("name", args['input'].replace('/', '_'))

images = get_images(args["input"])
results = get_results(images, sampling_method)
res_df = save_results(output_dir, base_name)

Statistics = Compare_Stacks(res_df)

if 'stats' in args.keys():
    Statistics.show_statistics()


# if __name__ == '__main__':
    # img_folders = [
    #     'data/stack_171220/Focadas/10X/',
    #     'data/stack_171220/Focadas/40X/',
    #     'data/stack_171220/laminula/10X/',
    #     'data/stack_171220/laminula/40X/',
    #     'data/stack_171220/out_of_focus/10X/',
    #     'data/stack_171220/out_of_focus/40X/'
    # ]

    # sampling_method = 'large_centroid'
    # IQA_Method = "LaplacianStd"

    # for img_folder in img_folders:
    #     images = []
    #     for img_file in os.listdir(img_folder):
    #         img_path = os.path.join(img_folder, img_file)
    #         images.append(img_path)


        # results = {}

        # for IQA_Method in [method for method in Analyzer.methods.keys()][-1]:
    #     results = {}
    #     method_results = [Analyzer.analyze(img, IQA_Method, sampling_method) for img in images]
    #     results[IQA_Method] = method_results
    #     # results[IQA_Method] = method_results

    # # method_results = [Analyzer.analyze(img, IQA_Method, sampling_method) for img in images]
    # # results[IQA_Method] = method_results

    #     res_df = pd.DataFrame.from_dict(results, orient = 'index')

    #     analysis_name = img_folder.replace('/', '_')
    #     output_dir = os.path.join(os.getcwd(), 'output/')

    #     res_df.to_csv(os.path.join(output_dir, 'bulk_{}_{}_{}_results.csv'.format(analysis_name, IQA_Method, sampling_method)), index= None)

